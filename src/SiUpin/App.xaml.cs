﻿using System;
using SiUpin.Views.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SiUpin
{
    public partial class App : Application
    {
        public App()
        {
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider
                .RegisterLicense("MzI4ODQ5QDMxMzgyZTMzMmUzME5GS2JQK0pJZWhPMDhXOGUwNUtTVkQwTm1mYko3dFJ5eVNNNTdzWDN4alE9");

            InitializeComponent();

            MainPage = new SimpleLoginPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}